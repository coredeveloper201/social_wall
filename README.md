t17-social-wall-api
===================

### Import MongoDB

```
mongorestore
```

### API-Documentation:

```
/doc
```

### Auth

Basic Auth:
username: admin
password: admin

### AppInstances (Are different clients) in Database:

* 5lbo1cmd
* o0z5gl15


## Some example API-calls
### Call to get all published walls of 5lbo1cmd

```
curl -X GET \
  http://127.0.0.1:8000/5lbo1cmd/walls \
  -H 'cache-control: no-cache'
```

### Call to get all walls of 5lbo1cmd (authenticated)

```
curl -X GET \
  http://127.0.0.1:8000/5lbo1cmd/walls \
  -H 'accept: application/vnd.social-wall.backend+json' \
  -H 'authorization: Basic YWRtaW46YWRtaW4=' \
  -H 'cache-control: no-cache'
```

### Add new wall to 5lbo1cmd (minimal data)

```
curl -X POST \
  http://localhost:8000/5lbo1cmd/walls \
  -H 'accept: application/vnd.social-wall.backend+json' \
  -H 'authorization: Basic YWRtaW46YWRtaW4=' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{"wall": {"name": "test", "intro": {"de": "intro"}, "mainHashtag": "test"}}'
```