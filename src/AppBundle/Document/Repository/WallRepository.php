<?php

namespace AppBundle\Document\Repository;

use Trick17\ApiBundle\Model\DocumentRepository;

class WallRepository extends DocumentRepository
{
    protected $queryFields = array(
        'name',
        'mainHashtag',
        'hashtags',
    );

    public function getAppInstances()
    {
        return $this->createQueryBuilder()
            ->distinct('appInstance')
            ->getQuery()
            ->execute();
    }

    public function findHashtags($wallId = null, $hashtag = null, $appInstance = null, $excludeWithAllFacebookPosts = false)
    {
        $query = $this->createQueryBuilder();

        if (!empty($wallId)) {
            $query->field('id')->equals($wallId);
        }

        if (!empty($appInstance)) {
            $query->field('appInstance')->equals($appInstance);
        }

        if ($excludeWithAllFacebookPosts) {
            $query->field('allFacebookPosts')->equals(false);
        }

        $hashtags = array();
        foreach ($query->getQuery()->execute() as $wall) {
            if (!empty($hashtag)) {
                $hashtags[$hashtag][] = $wall;
                continue;
            }

            if (!isset($hashtags[$wall->getMainHashtag()]) || !in_array($wall, $hashtags[$wall->getMainHashtag()])) {
                $hashtags[$wall->getMainHashtag()][] = $wall;
            }
            if ($wall->getHashtags()) {
                foreach (explode(',', $wall->getHashtags()) as $wallHashtag) {
                    if (!empty($wallHashtag) && (!isset($hashtags[$wallHashtag]) || !in_array($wall, $hashtags[$wallHashtag]))) {
                        $hashtags[$wallHashtag][] = $wall;
                    }
                }
            }
        }

        return $hashtags;
    }

    public function findWithAllFacebookPosts($wallId, $appInstance)
    {
        $query = parent::findRecordsQuery($appInstance, null, null);

        if (!empty($wallId)) {
            $query->field('id')->equals($wallId);
        }

        $query->field('allFacebookPosts')->equals(true);

        return $query->getQuery()->execute();
    }
}
