<?php

namespace AppBundle\Document;

use AppBundle\Validator\Constraints as AppAssert;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as Serializer;
use Trick17\ApiBundle\Model\BaseObject;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @MongoDB\Document(repositoryClass="AppBundle\Document\Repository\WallRepository")
 *
 * @Gedmo\Loggable
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 *
 * @Serializer\ExclusionPolicy("all")
 */
class Wall extends BaseObject
{
    /**
     * @var int
     *
     * @MongoDB\Id
     *
     * @Serializer\Expose
     * @Serializer\Groups({"backend", "public"})
     */
    private $id;

    /**
     * @var string
     *
     * @MongoDB\String
     *
     * @Assert\NotBlank()
     *
     * @Gedmo\Versioned
     *
     * @Serializer\Expose
     * @Serializer\Groups({"backend"})
     */
    private $name;

    /**
     * @var array
     *
     * @MongoDB\Hash
     *
     * @AppAssert\ValidLanguageArrayKeys()
     *
     * @Gedmo\Versioned
     *
     * @Serializer\Expose
     * @Serializer\Groups({"backend"})
     */
    private $intro;

    /**
     * @var string
     *
     * @MongoDB\String
     *
     * @Assert\NotBlank()
     *
     * @Gedmo\Versioned
     *
     * @Serializer\Expose
     * @Serializer\Groups({"backend", "public"})
     */
    private $mainHashtag;

    /**
     * @var array
     *
     * @MongoDB\String
     *
     * @Gedmo\Versioned
     */
    private $hashtags;

    /**
     * @var bool
     *
     * @MongoDB\Boolean
     *
     * @Gedmo\Versioned
     *
     * @Serializer\Expose
     * @Serializer\Groups({"backend"})
     */
    private $allFacebookPosts = false;

     /**
      * @var string
      *
      * @MongoDB\String
      * @MongoDB\Index
      *
      * @Assert\NotBlank()
      */
     private $appInstance;

    /**
     * @var
     *
     * @MongoDB\Date
     *
     * @Gedmo\Timestampable(on="create")
     *
     * @Serializer\Expose
     * @Serializer\Type("DateTime<'Y-m-d\TH:i:sP'>")
     */
    private $createdAt;

    /**
     * @var
     *
     * @MongoDB\Date
     *
     * @Gedmo\Timestampable
     *
     * @Serializer\Expose
     * @Serializer\Type("DateTime<'Y-m-d\TH:i:sP'>")
     */
    private $updatedAt;

    /**
     * @var string
     *
     * @MongoDB\String
     *
     * @Gedmo\Blameable(on="create")
     */
    private $createdBy;

    /**
     * @var string
     *
     * @MongoDB\String
     *
     * @Gedmo\Blameable
     */
    private $updatedBy;

    /**
     * @var
     *
     * @MongoDB\Date
     */
    private $deletedAt;

    /**
     * Get id.
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set intro.
     *
     * @param array $intro
     *
     * @return $this
     */
    public function setIntro($intro)
    {
        $this->intro = $intro;

        return $this;
    }

    /**
     * Get intro.
     *
     * @return array $intro
     */
    public function getIntro()
    {
        return $this->intro;
    }

    /**
     * Set mainHashtag.
     *
     * @param string $mainHashtag
     *
     * @return self
     */
    public function setMainHashtag($mainHashtag)
    {
        $this->mainHashtag = $mainHashtag;

        return $this;
    }

    /**
     * Get mainHashtag.
     *
     * @return string $mainHashtag
     */
    public function getMainHashtag()
    {
        return $this->mainHashtag;
    }

    /**
     * Set hashtags.
     *
     * @param string $hashtags
     *
     * @return self
     */
    public function setHashtags($hashtags)
    {
        $this->hashtags = $hashtags;

        return $this;
    }

    /**
     * Get hashtags.
     *
     * @return string $hashtags
     */
    public function getHashtags()
    {
        return $this->hashtags;
    }

    /**
     * Set allFacebookPosts.
     *
     * @param bool $allFacebookPosts
     *
     * @return $this
     */
    public function setAllFacebookPosts($allFacebookPosts)
    {
        $this->allFacebookPosts = $allFacebookPosts;

        return $this;
    }

    /**
     * Get allFacebookPosts.
     *
     * @return bool $allFacebookPosts
     */
    public function getAllFacebookPosts()
    {
        return $this->allFacebookPosts;
    }

    /**
     * Set appInstance.
     *
     * @param string $appInstance
     *
     * @return self
     */
    public function setAppInstance($appInstance)
    {
        $this->appInstance = $appInstance;

        return $this;
    }

    /**
     * Get appInstance.
     *
     * @return string $appInstance
     */
    public function getAppInstance()
    {
        return $this->appInstance;
    }

    /**
     * Set createdAt.
     *
     * @param date $createdAt
     *
     * @return self
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return date $createdAt
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param date $updatedAt
     *
     * @return self
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return date $updatedAt
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set createdBy.
     *
     * @param string $createdBy
     *
     * @return self
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy.
     *
     * @return string $createdBy
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy.
     *
     * @param string $updatedBy
     *
     * @return self
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy.
     *
     * @return string $updatedBy
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set deletedAt.
     *
     * @param date $deletedAt
     *
     * @return self
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt.
     *
     * @return date $deletedAt
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    //Custom

    /**
     * @Serializer\VirtualProperty
     * @Serializer\Groups({"backend"})
     * @Serializer\SerializedName("hashtags")
     *
     * @return array
     */
    public function getHashtagsArray()
    {
        if (empty($this->hashtags)) {
            return array();
        }

        return explode(',', $this->hashtags);
    }

    public function getAllHashtags()
    {
        $hashtags = array($this->mainHashtag);
        if (!empty($this->hashtags)) {
            $hashtags = array_merge($hashtags, explode(',', $this->hashtags));
        }

        return $hashtags;
    }
}
