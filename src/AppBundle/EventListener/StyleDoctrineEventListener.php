<?php

namespace AppBundle\EventListener;

use JMS\DiExtraBundle\Annotation as DI;
use Doctrine\ODM\MongoDB\Event\PreUpdateEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Document\Style;
use Trick17\ApiBundle\Exception\Error;
use Trick17\ApiBundle\Exception\ErrorException;

/**
 * @DI\Service
 * @DI\Tag("doctrine_mongodb.odm.event_listener", attributes = {"event" = "preUpdate"})
 */
class StyleDoctrineEventListener
{
    /**
     * @DI\InjectParams({
     *	"container" = @DI\Inject("service_container"),
     * })
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function preUpdate(PreUpdateEventArgs $event)
    {
        if ($event->getObject() instanceof Style) {
            if ($this->hasWidgetStyleChanged($event)) {
                $this->widgetChanged($event);
            }
        }
    }

    private function hasWidgetStyleChanged($event)
    {
        $widgetFields = array(
            'globalTextColor',
            'primaryColor',
            'primaryBackgroundColor',
            'buttonColor',
            'buttonBackgroundColor',
            'extra',
        );
        foreach ($widgetFields as $widgetField) {
            if ($event->hasChangedField($widgetField)) {
                return true;
            }
        }
    }

    private function widgetChanged($event)
    {
        $style = $event->getDocument();
        $styleGenerator = $this->container->get('app_bundle.style.style_generator');
        try {
            $styleGenerator->generateWidgetStyle($style);
        } catch (\Exception $e) {
            $apiError = new Error(Error::ERROR_CODE_VALIDATION_ERROR);
            $apiError->set('errors', array('extra' => $e->getMessage()));
            throw new ErrorException($apiError);
        }
    }
}
