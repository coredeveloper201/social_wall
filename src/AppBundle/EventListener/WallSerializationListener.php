<?php

namespace AppBundle\EventListener;

use JMS\DiExtraBundle\Annotation as DI;
use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use JMS\Serializer\EventDispatcher\ObjectEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Add data after serialization.
 *
 * @DI\Service()
 * @DI\Tag("jms_serializer.event_subscriber")
 */
class WallSerializationListener implements EventSubscriberInterface
{
    /**
     * @DI\InjectParams({
     *  "requestStack" = @DI\Inject("request_stack")
     * })
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->request = $requestStack->getMasterRequest();
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            array(
                'event' => 'serializer.post_serialize',
                'class' => 'AppBundle\Document\Wall',
                'method' => 'onPostSerialize'
            ),
        );
    }

    public function onPostSerialize(ObjectEvent $event)
    {
        if (!$this->request instanceof Request) {
            // don't do anything if it's not a request
            return;
        }

        $requestAttributes = $this->request->attributes;
        if ($requestAttributes->has('media_type') && in_array('public', $requestAttributes->get('media_type'))) {
            $locale = $this->request->getLocale();
            $wallIntros = $event->getObject()->getIntro();

            $intro = '';
            if (array_key_exists($locale, (array) $wallIntros)) {
                $intro = $wallIntros[$locale];
            }

            $event->getVisitor()->addData('intro', $intro);
        }
    }
}
