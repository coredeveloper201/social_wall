<?php

namespace AppBundle\EventListener;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Trick17\SettingsBundle\Event\SettingEvent;

/**
 * @DI\Service
 * @DI\Tag("kernel.event_subscriber")
 */
class SettingEventListener implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return array(
            SettingEvent::UPDATE => 'onSettingUpdate',
        );
    }

    public function onSettingUpdate(SettingEvent $event)
    {
        $setting = $event->getRecord();
        if ($setting->getSettingManager()->getKey() === 'facebook-page' && !$setting->getValue()) {
            $settings = $event->container->get('settings');
            $settings->set('facebook-access-token', null);
            $settings->set('facebook-name', null);
            $settings->set('facebook-picture', null);
            $settings->set('facebook-link', null);
        }

        if ($setting->getSettingManager()->getKey() === 'facebook-access-token') {
            $facebook = $event->container->get('social.status.facebook');
            $facebook->updateAccessToken($setting->getValue());
        }
    }
}
