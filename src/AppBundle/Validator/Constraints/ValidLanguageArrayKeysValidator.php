<?php

namespace AppBundle\Validator\Constraints;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * @DI\Service()
 * @DI\Tag("validator.constraint_validator")
 */
class ValidLanguageArrayKeysValidator extends ConstraintValidator
{
    const VALID_LANGUAGE_KEYS = ['de', 'it', 'en'];

    public function validate($property, Constraint $constraint)
    {
        if (is_array($property)) {
            $notValidKeys = array_diff(array_keys($property), self::VALID_LANGUAGE_KEYS);
            if ($notValidKeys) {
                $this->context->buildViolation($constraint->notValidKeysMessage)->addViolation();
            }
        } else {
            $this->context->buildViolation($constraint->mustBeArrayMessage)->addViolation();
        }
    }
}
