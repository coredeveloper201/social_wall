<?php

namespace AppBundle\Style;

use JMS\DiExtraBundle\Annotation as DI;
use MatthiasMullie\Minify;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @DI\Service()
 */
class StyleGenerator
{
    /**
     * @DI\InjectParams({
     *	"container" = @DI\Inject("service_container"),
     * })
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function generateWidgetStyle($style)
    {
        $this->generateStyle($style);
    }

    private function generateStyle($style)
    {
        require_once $this->container->get('kernel')->getRootDir().'/../vendor/oyejorge/less.php/lessc.inc.php';
        $widgetDir = $this->container->get('kernel')->getRootDir().'/../src/AppBundle/Resources/public/widget';
        $lessCompiler = new \lessc();
        $minifier = new Minify\CSS();

        $releases = array_filter(glob($widgetDir.'/*'), 'is_dir');
        foreach ($releases as $releaseDir) {
            $release = basename($releaseDir);
            $less = $this->getLess($style);
            $lessCompiler->setImportDir(array($releaseDir));
            $css = $lessCompiler->compile($less);
            $minifiedCss = $minifier->add($css)->minify();

            $s3 = $this->container->get('aws.s3');
            $s3Key = 'widget/instance/'.$style->getAppInstance().'/'.$release.'/widget.css';
            $s3->saveContent($s3Key, $minifiedCss);
        }
    }

    private function getLess($style)
    {
        $variables = array(
            'global-text-color',
            'primary-color',
            'primary-background-color',
            'button-color',
            'button-background-color',
        );
        $less = '@import "theme.less";'.PHP_EOL;
        foreach ($variables as $variable) {
            $less .= $this->getLessVariable($variable, $style);
        }
        $less .= '@import "app.less";'.PHP_EOL;
        $less .= $style->getExtra().PHP_EOL;

        return $less;
    }

    private function getLessVariable($variable, $style)
    {
        $method = 'get'.str_replace(' ', '', ucwords(str_replace('-', ' ', $variable)));
        if ($style->$method()) {
            return '@'.$variable.': '.$style->$method().';'.PHP_EOL;
        }
    }
}
