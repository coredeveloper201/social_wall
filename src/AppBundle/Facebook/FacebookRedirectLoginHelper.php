<?php

namespace AppBundle\Facebook;

use Facebook\FacebookRedirectLoginHelper as BaseFacebookRedirectLoginHelper;

class FacebookRedirectLoginHelper extends BaseFacebookRedirectLoginHelper
{
    /**
   * Check if a redirect has a valid state.
   *
   * @return bool
   */
  protected function isValidRedirect()
  {
      return $this->getCode();
  }
}
