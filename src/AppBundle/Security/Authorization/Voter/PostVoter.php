<?php

namespace AppBundle\Security\Authorization\Voter;

use JMS\DiExtraBundle\Annotation as DI;
use Trick17\ApiBundle\Security\Authorization\Voter\MainBackendVoter;

/**
 * @DI\Service
 * @DI\Tag("security.voter")
 */
class PostVoter extends MainBackendVoter
{
    protected $class = 'AppBundle\Document\Post';

    protected function anonymousAllowed($attributes, $object)
    {
        if (in_array('ROLE_SHOW', $attributes) && count($attributes) === 1) {
            return true;
        }

        return false;
    }
}
