<?php

namespace AppBundle\Security\Authorization\Voter;

use JMS\DiExtraBundle\Annotation as DI;
use Trick17\ApiBundle\Security\Authorization\Voter\MainBackendVoter;

/**
 * @DI\Service
 * @DI\Tag("security.voter")
 */
class StyleVoter extends MainBackendVoter
{
    protected $class = 'AppBundle\Document\Style';
}
