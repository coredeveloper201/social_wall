<?php

namespace AppBundle\Controller;

use Trick17\ApiBundle\Controller\Controller;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Trick17\ApiBundle\Controller\Annotations\Trick17ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as RestMethod;
use Trick17\ApiBundle\Exception\Error;
use Trick17\ApiBundle\Exception\ErrorException;

class PostController extends Controller
{
    /**
     * @ApiDoc
     * @Trick17ApiDoc
     */
    public function cgetAction(Request $request)
    {
        return parent::cgetAction($request);
    }

    /**
     * @ApiDoc
     * @Trick17ApiDoc
     * @RestMethod\Put("/posts/{id}/disable")
     */
    public function disableAction($id, Request $request)
    {
        $modelHelper = $this->getModelHelper($request);
        $record = $modelHelper->getRecord($id);
        $modelHelper->isGranted('ROLE_EDIT', $record);

        if ($record->getDisabled()) {
            $apiError = new Error(Error::ERROR_CODE_VALIDATION_ERROR);
            throw new ErrorException($apiError);
            $apiError->set('errors', 'Alreay disabled');
        }
        $record->setDisabled(true);
        $modelHelper->getDoctrineManager()->persist($record);
        $modelHelper->getDoctrineManager()->flush();

        return $this->view($request, $record);
    }

    /**
     * @ApiDoc
     * @Trick17ApiDoc
     * @RestMethod\Put("/posts/{id}/enable")
     */
    public function enableAction($id, Request $request)
    {
        $modelHelper = $this->getModelHelper($request);
        $record = $modelHelper->getRecord($id);
        $modelHelper->isGranted('ROLE_EDIT', $record);

        if (!$record->getDisabled()) {
            $apiError = new Error(Error::ERROR_CODE_VALIDATION_ERROR);
            throw new ErrorException($apiError);
            $apiError->set('errors', 'Alreay enabled');
        }
        $record->setDisabled(false);
        $modelHelper->getDoctrineManager()->persist($record);
        $modelHelper->getDoctrineManager()->flush();

        return $this->view($request, $record);
    }
}
