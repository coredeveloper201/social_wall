<?php

namespace AppBundle\Controller;

use Trick17\ApiBundle\Controller\Controller;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Trick17\ApiBundle\Controller\Annotations\Trick17ApiDoc;
use Symfony\Component\HttpFoundation\Request;

class WallController extends Controller
{
    /**
     * @ApiDoc
     * @Trick17ApiDoc
     */
    public function cgetAction(Request $request)
    {
        return parent::cgetAction($request);
    }

    /**
     * @ApiDoc
     * @Trick17ApiDoc
     */
    public function getAction($id, Request $request)
    {
        return parent::getAction($id, $request);
    }

    /**
     * @ApiDoc
     * @Trick17ApiDoc
     */
    public function cpostAction(Request $request)
    {
        return parent::cpostAction($request);
    }

    /**
     * @ApiDoc
     * @Trick17ApiDoc
     */
    public function putAction($id, Request $request)
    {
        return parent::putAction($id, $request);
    }

    /**
     * @ApiDoc
     * @Trick17ApiDoc
     */
    public function patchAction($id, Request $request)
    {
        return parent::patchAction($id, $request);
    }

    /**
     * @ApiDoc
     * @Trick17ApiDoc
     */
    public function deleteAction($id, Request $request)
    {
        return parent::deleteAction($id, $request);
    }
}
