<?php

namespace AppBundle\Controller\Social;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations as RestMethod;
use Symfony\Component\HttpFoundation\Request;

class StatusController extends Controller
{
    /**
     * @ApiDoc
     * @RestMethod\Get("/social/status")
     */
    public function getStatusAction(Request $request)
    {
        $status = array();

        $facebookStatus = $this->container->get('social.status.facebook');

        $status[] = $facebookStatus->getStatusResponse();

        return $status;
    }
}
