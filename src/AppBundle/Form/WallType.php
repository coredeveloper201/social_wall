<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Form\DataTransformer\StringToArrayTransformer;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class WallType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $arrayTransformer = new StringToArrayTransformer();

        $builder
            ->add('name', TextType::class)
            ->add('intro', TextType::class)
            ->add('mainHashtag', TextType::class)
            ->add($builder->create('hashtags', TextType::class)->addModelTransformer($arrayTransformer))
            ->add('allFacebookPosts', TextType::class)
            ->add('appInstance', TextType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Document\Wall',
        ));
    }

    public function getBlockPrefix()
    {
        return 'wall';
    }
}
