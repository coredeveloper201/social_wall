<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class StyleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('globalTextColor', TextType::class)
            ->add('primaryColor', TextType::class)
            ->add('primaryBackgroundColor', TextType::class)
            ->add('buttonColor', TextType::class)
            ->add('buttonBackgroundColor', TextType::class)
            ->add('extra', TextType::class)
            ->add('appInstance', TextType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Document\Style',
        ));
    }

    public function getBlockPrefix()
    {
        return 'style';
    }
}
