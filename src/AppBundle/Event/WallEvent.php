<?php

namespace AppBundle\Event;

use Trick17\ApiBundle\Event\MainEvent;

class WallEvent extends MainEvent
{
    const CREATE = 'wall.onCreate';
    const DELETE = 'wall.onDelete';
}
