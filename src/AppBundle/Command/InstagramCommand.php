<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use MetzWeb\Instagram\Instagram;
use AppBundle\Document\Post;

class InstagramCommand extends Command
{
    const CHANNEL = 'instagram';

    protected $container;

    protected function configure()
    {
        $this
            ->setName('trick17.social_wall:instagram')
            ->setDescription('Instagram import')
            ->addArgument(
                'wall',
                InputArgument::OPTIONAL,
                'Id of wall'
            )
            ->addArgument(
                'hashtag',
                InputArgument::OPTIONAL,
                'Hashtag'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getApplication()->getKernel()->getContainer();
        $dm = $container->get('t17_doctrine')->getManager();
        $exceptions = array();

        $instagram = new Instagram(array(
            'apiKey' => '',
            'apiSecret' => '',
            'apiCallback' => '',
        ));

        $instagram->setAccessToken($container->getParameter('instagram_access_token'));

        $hashtags = $dm->getRepository('AppBundle:Wall')->findHashtags($input->getArgument('wall'), $input->getArgument('hashtag'));
        foreach ($hashtags as $hashtag => $walls) {
            try {
                $result = $instagram->getTagMedia($this->removeHash($hashtag));
                if ($result->meta && $result->meta->code && $result->meta->code !== 200) {
                    throw new \Exception($result->meta->error_message);
                }
            } catch (\Exception $e) {
                $exceptions[] = $e->getMessage().' for '.$hashtag;
                continue;
            }
            if (!isset($result->data) || empty($result->data)) {
                continue;
            }
            foreach ($result->data as $instagramPost) {
                $postId = $instagramPost->id;
                foreach ($walls as $wall) {
                    $post = $dm->getRepository('AppBundle:Post')->findPost($postId, self::CHANNEL, $wall);
                    if (!$post) {
                        $post = new Post();
                        $post->setPostId($postId);
                        $post->setChannel(self::CHANNEL);
                        $post->setWall($wall);
                        $post->setAppInstance($wall->getAppInstance());

                        $post->setLink($instagramPost->link);

                        $post->addHashtag($hashtag);

                        $createdAt = date_create()->setTimestamp($instagramPost->created_time);
                        $createdAt->setTimezone(new \DateTimeZone('Europe/Rome'));
                        $post->setCreatedAt($createdAt);
                    }

                    if (!$post->getHashtags() || !in_array($hashtag, $post->getHashtags())) {
                        $post->addHashtag($hashtag);
                    }

                    $post->setImageUrl($instagramPost->images->standard_resolution->url);
                    $post->setUserName($instagramPost->user->username);
                    $post->setUserProfileImageUrl($instagramPost->user->profile_picture);
                    $post->setUserLink('https://www.instagram.com/'.$instagramPost->user->username);

                    $dm->persist($post);
                }
                $dm->flush();
            }
        }

        if (!empty($exceptions)) {
            throw new \Exception(implode(', ', $exceptions));
        }
    }

    private function removeHash($hashtag)
    {
        return str_replace('#', '', $hashtag);
    }
}
