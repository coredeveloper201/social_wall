<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\ArrayInput;

class ImportAllCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('trick17.social_wall:import_all')
            ->setDescription('Import from all social media')
            ->addArgument(
                'wall',
                InputArgument::OPTIONAL,
                'Id of wall'
            )
            ->addArgument(
                'hashtag',
                InputArgument::OPTIONAL,
                'Hashtag'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getApplication()->getKernel()->getContainer();
        $dm = $container->get('t17_doctrine')->getManager();
        $pusher = $container->get('lopi_pusher.pusher');
        $app = $this->getApplication();
        $exceptions = array();

        $in = new ArrayInput(array(
            'command' => 'trick17.social_wall:facebook',
            'wall' => $input->getArgument('wall'), 'hashtag' => $input->getArgument('hashtag'),
        ));
        try {
            $app->doRun($in, $output);
        } catch (\Exception $e) {
            $exceptions[] = $e->getMessage();
        }

        $in = new ArrayInput(array(
            'command' => 'trick17.social_wall:instagram',
            'wall' => $input->getArgument('wall'), 'hashtag' => $input->getArgument('hashtag'),
        ));
        try {
            $app->doRun($in, $output);
        } catch (\Exception $e) {
            $exceptions[] = $e->getMessage();
        }
        $in = new ArrayInput(array(
            'command' => 'trick17.social_wall:twitter',
            'wall' => $input->getArgument('wall'), 'hashtag' => $input->getArgument('hashtag'),
        ));
        try {
            $app->doRun($in, $output);
        } catch (\Exception $e) {
            $exceptions[] = $e->getMessage();
        }

        $appInstances = array();
        $hashtags = $dm->getRepository('AppBundle:Wall')->findHashtags($input->getArgument('wall'), $input->getArgument('hashtag'));
        foreach ($hashtags as $hashtag => $walls) {
            foreach ($walls as $wall) {
                if (!in_array($wall->getAppInstance(), $appInstances)) {
                    $appInstances[] = $wall->getAppInstance();
                }
            }
        }

        foreach ($appInstances as $appInstance) {
            $pusher->trigger('presence-appinstance-'.$appInstance, 'new_posts', array());
        }

        if (!empty($exceptions)) {
            throw new \Exception(implode(', ', $exceptions));
        }
    }
}
