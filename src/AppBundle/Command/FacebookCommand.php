<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\FacebookRequestException;
use AppBundle\Document\Post;

class FacebookCommand extends Command
{
    const CHANNEL = 'facebook';

    protected $dm;

    protected function configure()
    {
        $this
            ->setName('trick17.social_wall:facebook')
            ->setDescription('Facebook import')
            ->addArgument(
                'wall',
                InputArgument::OPTIONAL,
                'Id of wall'
            )
            ->addArgument(
                'hashtag',
                InputArgument::OPTIONAL,
                'Hashtag'
            )
            ->addArgument(
                'pusher',
                InputArgument::OPTIONAL,
                'Trigger pusher on finish',
                false
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getApplication()->getKernel()->getContainer();
        $dm = $container->get('t17_doctrine')->getManager();
        $this->dm = $dm;

        $settings = $container->get('settings');
        $facebookStatus = $container->get('social.status.facebook');
        $appId = $container->getParameter('facebook_app_id');
        $appSecret = $container->getParameter('facebook_app_secret');
        $exceptions = array();
        $inputWallId = $input->getArgument('wall');
        $inputHashTag = $input->getArgument('hashtag');

        $inputPusher = $input->getArgument('pusher');
        $pusher = null;
        if ($inputPusher) {
            $pusher = $container->get('lopi_pusher.pusher');
        }

        $appInstances = $dm->getRepository('AppBundle:Wall')->getAppInstances();

        foreach ($appInstances as $appInstance) {
            $options = array('appInstance' => $appInstance);
            $settings->init($options);
            if (!$settings->get('facebook-page')) {
                continue;
            }
            $facebookStatus->init($options);
            if (!$facebookStatus->getStatus()) {
                continue;
            }

            $accessToken = $settings->get('facebook-access-token');
            FacebookSession::setDefaultApplication($appId, $appSecret);
            $session = new FacebookSession($accessToken);
            $facebookPageId = $settings->get('facebook-page');

            try {
                $response = (
                    new FacebookRequest(
                        $session,
                        'GET',
                        '/'.$facebookPageId.'/posts',
                        array('fields' => 'message,created_time,link,full_picture,from,attachments', 'limit' => 100),
                        'v2.6'
                    )
                )->execute()->getGraphObject()->asArray();
            } catch (FacebookRequestException $e) {
                $exceptions[] = $e->getMessage().' for '.$facebookPageId;
                continue;
            } catch (\Exception $e) {
                $exceptions[] = $e->getMessage().' for '.$facebookPageId;
                continue;
            }

            $wallsByHashTag = $dm->getRepository('AppBundle:Wall')->findHashtags($inputWallId, $inputHashTag, $appInstance, true);
            $wallsWithAllFacebookPosts = $dm->getRepository('AppBundle:Wall')->findWithAllFacebookPosts($inputWallId, $appInstance);

            foreach ($response['data'] as $facebookPost) {
                if (!isset($facebookPost->message)) {
                    continue;
                }

                preg_match_all('/#([^\s]+)/', $facebookPost->message, $postHashTags);

                foreach ($wallsWithAllFacebookPosts as $wall) {
                    $post = $this->savePost($facebookPost, $wall, $facebookPageId);

                    foreach ($postHashTags[0] as $postHashTag) {
                        if (in_array($postHashTag, $wall->getAllHashtags()) && (!$post->getHashtags() || !in_array($postHashTag, $post->getHashtags()))) {
                            $post->addHashtag($postHashTag);
                        }
                    }

                    $dm->persist($post);
                }

                foreach ($postHashTags[0] as $postHashTag) {
                    if (in_array($postHashTag, array_keys($wallsByHashTag))) {
                        foreach ($wallsByHashTag[$postHashTag] as $wall) {
                            if ($wall->getAllFacebookPosts()) {
                                continue;
                            }
                            $post = $this->savePost($facebookPost, $wall, $facebookPageId);

                            if (!$post->getHashtags() || !in_array($postHashTag, $post->getHashtags())) {
                                $post->addHashtag($postHashTag);
                            }

                            $dm->persist($post);
                        }
                        $dm->flush();
                    }
                }
                $dm->flush();
            }

            if ($inputPusher) {
                $pusher->trigger('presence-appinstance-'.$appInstance, 'new_posts', array());
            }
        }

        if (!empty($exceptions)) {
            throw new \Exception(implode(', ', $exceptions));
        }
    }

    private function savePost($facebookPost, $wall, $facebookPageId)
    {
        $postId = $facebookPost->id;
        $post = $this->dm->getRepository('AppBundle:Post')->findPost($postId, self::CHANNEL, $wall);

        if (!$post) {
            list($facebookPageId, $facebookId) = explode('_', $postId);
            $post = new Post();
            $post->setPostId($postId);
            $post->setChannel(self::CHANNEL);
            $post->setWall($wall);
            $post->setAppInstance($wall->getAppInstance());

            if (isset($facebookPost->message)) {
                $post->setText($facebookPost->message);
            }
            $post->setLink('https://www.facebook.com/permalink.php?story_fbid='.$facebookId.'&id='.$facebookPageId);
            $post->setUserLink('https://www.facebook.com/'.$facebookPageId.'/');

            $createdAt = date_create($facebookPost->created_time);
            $createdAt->setTimezone(new \DateTimeZone('Europe/Rome'));
            $post->setCreatedAt($createdAt);
        }

        $post->setUserName($facebookPost->from->name);
        $post->setUserProfileImageUrl('https://graph.facebook.com/'.$facebookPageId.'/picture');

        $firstAttachmentImageUrl = $this->getFirstAttachmentImageUrl($facebookPost);
        if (isset($firstAttachmentImageUrl)) {
            $post->setImageUrl($firstAttachmentImageUrl);
        } else if (isset($facebookPost->full_picture)) {
            $post->setImageUrl($facebookPost->full_picture);
        }

        return $post;
    }

    private function getFirstAttachmentImageUrl($facebookPost)
    {
        if (isset($facebookPost->attachments->data[0]->subattachments->data[0]->media->image->src)) {
            return $facebookPost->attachments->data[0]->subattachments->data[0]->media->image->src;
        }
    }
}
