## 1.3.0 (2017-04-18)

#### Neue Funktionen

* Einstellung pro Wall um alle Facebook-Beiträge anzuzeigen
* Erstes Bild bei Facebook-Karussell verwenden

---

## 1.2.0 (2016-04-26)

#### Refactor

* Symfony 3.0

---

## 1.0.0 (2016-03-29)

#### Neue Funktionen

* Erster Onlinegang von Social Wall

---
